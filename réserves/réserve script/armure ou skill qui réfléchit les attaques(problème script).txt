=begin
#==============================================================================#  
    Hima's Elemental Reflect v. 1.3
#==============================================================================#  

  Using this script will allow a character or enemy to reflect skills by
  element. 
  
  ---Version History:

  1.0 -  First Released
  1.1 -  Fixed infinite reflect problem
  1.2 -  More features added
          a.) You can now choose to reflect any physical or magical skill.
          b.) Physical reflect will also reflect any basic attack.
          c.) Basic attack with element will also be reflected from elemental reflector.
  1.3 -  Fixed some bugs that occur when you don't put all the ELEMENTS you defined into the database.
#==============================================================================#
    Of course, all credits of this script go to HIMA, the presumable
    lady who made this script.
    
  contact : ninomiya_mako@hotmail.com
#==============================================================================#  
=end
module HIMA_REFLECT

#=====================================#
#  This is the part you should read.  #
#=====================================#
=begin

The structure:

          ELEMENTS = {"ELEM_REFLECT_ID" => ID_TO_REFLECT}

  "ELEM_REFLECT_ID" = The ID of the element you want to check or raise to 'A'.
                      This can effectively be named whatever you want,
                      though you should probably relate it to the proper element.
                      It must be within the quotes, though.
  
   ID_TO_REFLECT    = This is the ID of the element that will be reflected.

   
   So lets say this is an example:
   
          ELEMENTS = {"Fire Reflector" => 1}

  In this instance, the name of the element that will be reflecting is called:
  "Fire Reflector", when this element is checked (on armor or accessories) or
  raised to 'A' resistance (on enemies) then the element in ID 1 will be
  reflected.
#==============================================================================#  
  SPECIAL ELEMENT WORDS:
  
  "Physical" - using this term in place of the ID_TO_REFLECT will reflect all
               skills with an ATK-Force > 0 and even normal attacks.
            
  "Magical"  - Using this term in place of ID_TO_REFLECT will reflect all types
               of skills with an INT-Force > 0, regardless of element.

Lastly:
    
    WORD  - This is the string that will pop up when a skill is reflected.

Oh, and on a final note:
  If you have cast an element on an enemy, ie, Ice, and both you and the
  enemy reflects the ice element, it will just say "Block".
  
  You can also edit that word if you want to, just CTRL+F "Block", and change
  each one to whatever you want. :)
________________________________________________________________________________
=end
#========================================#
#  This is the tiny part you customize.  #
#========================================#

    ELEMENTS = { "a" => 1 }
    WORD = "Renvoy� !"
#========================================#
end

module RPG
    class Skill
        def type_of_skill
            if @int_f > 0
                return "Magical"
            elsif @atk_f > 0
                return "Physical"
            else
                return nil
            end
        end
    end
end

class Game_Battler
  attr_accessor :reflect                   # Reflect Flag
    alias hima_reflect_init initialize
    def initialize
        @reflect = 0    #0 = no reflect, 1 = basic_attack, 2 = skill_effect
    hima_reflect_init
    end
    
    alias hima_reflect_attack_effect attack_effect
    def attack_effect(attacker)
        # Check for reflect basic attack
        element_to_reflect = HIMA_REFLECT::ELEMENTS.index("Physical")
        reflect_id = $data_system.elements.index(element_to_reflect)
        if reflect_id != nil
            if self.element_reflect(reflect_id) == 1
                if attacker.element_reflect(reflect_id) == 1
                    self.damage = "Neutralis�"
                    self.reflect = 0
                else
                    self.damage = HIMA_REFLECT::WORD
                    self.reflect = 1
                end
                return false
            end
        end
        # End of reflect basic attack
        
        # Check for reflect element of basic attack
        for q in 0...attacker.element_set.size
            element_id = attacker.element_set[q]
            element_to_reflect = HIMA_REFLECT::ELEMENTS.index(element_id)
            if element_to_reflect != nil
                reflect_id = $data_system.elements.index(element_to_reflect)
                if self.element_reflect(reflect_id) == 1
          if attacker.element_reflect(reflect_id) == 1
              self.damage = "Block"
              self.reflect = 0
          else
              self.damage = HIMA_REFLECT::WORD
              self.reflect = 1
          end
          return false
                end
            end
        end #end for        
        # End of reflect element of basic attack
        hima_reflect_attack_effect(attacker)
    end
    
    alias hima_reflect_skill_effect skill_effect
  def skill_effect(user, skill)
        # Reflect physical or magic skill
        if skill.type_of_skill != nil
            element_id = skill.type_of_skill
            element_to_reflect = HIMA_REFLECT::ELEMENTS.index(element_id)
            if element_to_reflect != nil
                reflect_id = $data_system.elements.index(element_to_reflect)
                if reflect_id != nil
                    if self.element_reflect(reflect_id) == 1
                        if user.element_reflect(reflect_id) == 1
                                self.damage = "Block"
                                self.reflect = 0
                        else
                                self.damage = HIMA_REFLECT::WORD
                                self.reflect = 2
                        end
                        return false
                    end
                end
            end
        end
        
        # Reflect specific element
        for q in 0...skill.element_set.size
            element_id = skill.element_set[q]
            element_to_reflect = HIMA_REFLECT::ELEMENTS.index(element_id)
            if element_to_reflect != nil
                reflect_id = $data_system.elements.index(element_to_reflect)
                if reflect_id != nil
                    if self.element_reflect(reflect_id) == 1
                        if user.element_reflect(reflect_id) == 1
                                self.damage = "Block"
                                self.reflect = 0
                        else
                                self.damage = HIMA_REFLECT::WORD
                                self.reflect = 2
                        end
                        return false
                    end
                end
            end
        end #end for
        hima_reflect_skill_effect(user,skill)
  end
    
    def element_reflect(element_id)
        if self.is_a?(Game_Actor)
        result = $data_classes[self.class_id].element_ranks[element_id]

            for i in [@armor1_id, @armor2_id, @armor3_id, @armor4_id]
                armor = $data_armors[i]
                if armor != nil and armor.guard_element_set.include?(element_id)
                    result = 1
                end
            end
    
            for i in @states
                if $data_states[i].guard_element_set.include?(element_id)
                    result = 1
                end
            end
        else
            result = $data_enemies[self.enemy_id].element_ranks[element_id]
            for i in @states
                if $data_states[i].guard_element_set.include?(element_id)
                    result = 1
                end
            end
        end
    # Method end
    return result
  end
end

class Game_Enemy < Game_Battler
    attr_accessor        :enemy_id
end

class Scene_Battle
    alias hima_update_phase4_step5 update_phase4_step5
  def update_phase4_step5
        hima_update_phase4_step5
    reflect = 0
        for target in @target_battlers
          if target.reflect != 0
                reflect = target.reflect
                target.reflect = 0
      end
    end
        if reflect > 0
      @target_battlers = []
            @target_battlers.push(@active_battler)
            case reflect
                when 1
                    @active_battler.attack_effect(@active_battler)
                when 2
                    @active_battler.skill_effect(@active_battler,@skill)
            end #case
            @phase4_step = 4
        else
      @phase4_step = 6
    end
  end
end